# coding=utf-8
import sys
import logging
import random  as  random_number
from game_state.game_types import GameWoodGrave, GameWoodGraveDouble, \
    GamePickItem, GameWoodTree, GameGainItem, GamePickup, GameDigItem
from game_state.game_event import dict2obj, obj2dict
from game_actors_and_handlers.base import BaseActor
from itertools import chain
##############################
from ctypes import windll
import sys

stdout_handle = windll.kernel32.GetStdHandle(-11)
SetConsoleTextAttribute = windll.kernel32.SetConsoleTextAttribute
##############################
import time
import os.path
import os

logger = logging.getLogger(__name__)


class VisitingUsers(BaseActor):
    def perform_action(self):
        #logger.info(u"######### Идем к другу ###########")
        #go_to_friend = {"action":"gameState","locationId":"main","user":'201018303',"objId":None,"type":"gameState"}#{"id":8,"action":"gameState","objId":null,"locationId":"main","user":"144371056","type":"gameState"} #,"id":46667
        #friends = ['8477452','22656865','27505179','107183826','68030140','163206097']#'476111',
        #print str(options)
        options = self._get_options()
        curuser = options[0][0]
        if curuser != 'me root':
            friends = options[0][2]
        else:
            friends = eval(open('friendsid.txt').read())
        friends = ['[BOT]friend1', '[BOT]friend2'] + friends
        #friends = eval(open('friendsid.txt').read())
        #friends = (open('friendsid.txt').read()).split('","')
        #friends[0] = friends[0][2:]#не должно быть
        #friends[-1] = friends[-1][:-2]
        #print friends
        #option = eval(options[1])
        myid = options[0][1]
        shovels = options[1][1]
        favdecors = options[1][2]
        use_golden_shovels = options[2]
        if favdecors == None: favdecors = ['none']
        #print myid
        if friends == None: return
        if shovels == None:
            shovels = 0
        else:
            shovels = int(shovels)

        daily_shovels = 100


        #            Юдо              Чудо
        #friends = ['[BOT]friend1','[BOT]friend2'] + friends
        #friends = ['[BOT]friend1','[BOT]friend2','3007545077112975219','8042347255933873247','10855907046378316515','11447800101354338263','7847267122470904758','18318155742226352457','7262954456861857990','9744913910228622896','17218879470280527644','2078919142605154600','18361112867401008531','11320143558909242211','7847267122470904758','11320143558909242211','16303366921369276765','12470974660198948168','8042347255933873247','[BOT]friend1']
        objtypes = ['woodTree', 'stone', 'decoration', 'building']
        if not hasattr(self._get_game_state(), 'mylocids'):
            self._get_game_state().mylocids = []
            for glocinf in []:  #self._get_game_state().get_state().locationInfos:
                self._get_game_state().mylocids.append(glocinf.locationId)
        #myid = '39245930'
        if not os.path.isdir('.\subloc'): os.makedirs('.\subloc')
        if not os.path.isdir('.\counts'): os.makedirs('.\counts')
        if not os.path.isdir('.\counts\\' + curuser): os.makedirs('.\counts\\' + curuser)
        #ftime = time.localtime(os.path.getmtime('.\counts\\'+curuser+'\countfnyt.txt'))
        #print '%d : %d : %d'%(ftime.tm_year, ftime.tm_mon, ftime.tm_mday)
        #self._get_game_state().fdend = 1
        cfname = '.\counts\\' + curuser + '\countfnyt.txt'
        if os.path.isfile(cfname):  #это проходит проверка на устарелость вайла счетчика. новый день!!!
            ftime = time.localtime(os.path.getmtime(cfname)).tm_mday
            if ftime != time.localtime().tm_mday:
                os.remove(cfname)
                fcousl = '.\counts\\' + curuser + '\couisle.txt'
                if os.path.isfile(fcousl): os.remove(fcousl)
        self._get_game_state().shovel = 0
        if not hasattr(self._get_game_state(), 'countfnyt'):
            try:
                self._get_game_state().countfnyt = int(open('.\counts\\' + curuser + '\countfnyt.txt').read())
            except:
                self._get_game_state().countfnyt = 0
                open('.\counts\\' + curuser + '\countfnyt.txt', 'w').write('')
        if not hasattr(self._get_game_state(), 'frobjstat'): self._get_game_state().frobjstat = 0
        if not hasattr(self._get_game_state(), 'countnyt'): self._get_game_state().countnyt = 0
        if not hasattr(self._get_game_state(), 'couisle'):
            try:
                self._get_game_state().couisle = int(open('.\counts\\' + curuser + '\couisle.txt').read()) + 1
            except:
                self._get_game_state().couisle = 0
        if not hasattr(self._get_game_state(), 'sendNewYearGift'): self._get_game_state().sendNewYearGift = 0

        #print obj2dict(self._get_game_state())

        if not hasattr(self._get_game_state(), 'gameObjects') and self._get_game_state().countfnyt < len(
                friends) and self._get_game_state().frobjstat == 0:
            self._get_game_state().frobjstat = 1
            user = friends[self._get_game_state().countfnyt]
            try:
                isles = (open('.\subloc\\' + str(user) + '.txt').read()).split("\n")
            except:
                isles = ['main']
            try:
                isle = isles[self._get_game_state().couisle]
                print isle
            except:
                isle = 'main'
            if isle == '': isle = 'main'
            self.cprint(u'1######### Идем к другу^6_%s^7_%d/%d^6на^3_%s^1#########' % (
            user, self._get_game_state().countfnyt + 1, len(friends), isle))
            self._get_game_state().planeAvailable = True
            #,{"type":"players","action":"getInfo","players":[str(user)]}])
            print self._get_game_state().couisle
            if self._get_game_state().couisle >= len(isles):
                self._get_game_state().countfnyt += 1
                open('.\counts\\' + curuser + '\countfnyt.txt', 'w').write(str(self._get_game_state().countfnyt))
                self._get_game_state().couisle = 0
            else:
                self._get_game_state().couisle += 1
            open('.\counts\\' + curuser + '\couisle.txt', 'w').write(str(self._get_game_state().couisle))
            self._get_events_sender().send_game_events(
                [{"action": "gameState", "locationId": isle, "user": str(user), "objId": None, "type": "gameState"}])

        if hasattr(self._get_game_state(), 'gameObjects'):  # and 
            ft = 0
            #for object in self._get_game_state().gameObjects:
            #    open(str(friends[self._get_game_state().countfnyt])+'objects.txt', 'a').write(str(obj2dict(object))+"\n")

            #self._get_game_state().frobjstat = 0
            #open('objects.txt', 'a').write(str(friends[self._get_game_state().countfnyt])+"\n")
            #print "############### gameObjects #################"
            events = []
            countnyt = 0
            countmyg = 0
            countpickup = 0
            objssvl = []
            objssvl2 = []
            newyeartree = []

            if hasattr(self._get_game_state(), 'alldigged'):
                alldigged = 1
            else:
                alldigged = 0
            try:
                isles = (open('.\subloc\\' + str(friends[self._get_game_state().countfnyt]) + '.txt').read()).split(
                    "\n")
            except:
                isles = []
            for object in self._get_game_state().gameObjects:
                #open(str(friends[self._get_game_state().countfnyt])+'objects.txt', 'a').write(str(obj2dict(object))+"\n")
                try:
                    #if object.item == '@B_PEAR':
                    travels = self._get_item_reader().get(object.item[1:]).travels
                    for newloc in travels:
                        if newloc.location in self._get_game_state().mylocids:
                            #if hasattr(object, 'location'): 
                            if not newloc.location in isles:
                                open('.\subloc\\' + str(friends[self._get_game_state().countfnyt]) + '.txt', 'a').write(
                                    str(newloc.location) + "\n")
                except:
                    pass
                #if object.type == 'guardGrave': print u'############ Сторож !!! ############'

                if object.item == '@B_HOCKEY_AIRPLANE':
                    if self._get_game_state().planeAvailable and object.usedPlatesCount < 5 and len(self._get_game_state().get_state().remoteThanksgiving) < 100:
                        self.cprint(u'5Кладем билет в самолет')
                        self._get_events_sender().send_game_events([
                            {"itemId": "HOCKEY_BOX_01", "action": "remoteThanksgiving", "type": "item", "objId": object.id}])
                        self._get_game_state().planeAvailable = False
                if object.item == 'B_CANDY_TOWER':  #так должно быть достаточно
                    self.cprint(u'5Кладем канфету под сладкую башню')
                    self._get_events_sender().send_game_events([
                        {"itemId": "CANDYTOWER_BOX_1", "action": "remoteThanksgiving", "type": "item",
                         "objId": object.id}])
                if self._get_game_state().countnyt + countnyt < 148:
                    if hasattr(object, 'type'):
                        tf = 0

                        if not myid == None and object.type == 'newYearTree':
                            tf = 1
                            usrs = len(object.users)
                            #object.users = usrs
                            #open('newyeartree.txt', 'a').write(str(obj2dict(object))+"\n")
                        if tf == 1 and not hasattr(self._get_game_state(), 'nytend'):
                            #print (u" Долбим в ёЛки!!!").encode('cp866')
                            #open('gameObjects.txt', 'a').write(str(obj2dict(object))+"\n")
                            #open('newyeartree.txt', 'a').write(object.item + " UsersGift:" + str(len(object.users)) + "\n")
                            tf = 1
                            f = 0
                            #Ёлки разной ёмкости. указано не точно.
                            if object.item == u'@B_SPRUCE_SMOLL' and len(object.users) < 3: f = 1
                            if object.item == u'@B_SPRUCE_MIDDLE' and len(object.users) < 6: f = 1
                            if object.item == u'@B_SPRUCE_BIG' and len(object.users) < 15: f = 1
                            if object.item == u'@B_CANDY_TOWER' and len(object.users) < 6: f = 1
                            #if object.item == u'@B_BASKETS_EASTER_2' and len(object.users) < 7: f = 1
                            #if object.item == u'@B_BASKETS_EASTER_3' and len(object.users) < 3: f = 1
                            for user in object.users:
                                if user.id == myid:
                                    countmyg += 1  #print "MyGift"
                                    f = 0
                                    break

                            #if not check_no_my_gift(object.users): f = 1
                            if self._get_game_state().countnyt + countnyt > 151 or hasattr(self._get_game_state(),
                                                                                           'nyna'):
                                self._get_game_state().nytend = 1
                                print "################## END ####################"
                            try:
                                couCAKE = int(open('.\counts\\' + curuser + '\couCAKE.txt').read())
                            except:
                                couCAKE = 0
                            if f == 1 and couCAKE < 150:
                                print u'Ложим пряник'
                                newyeartree.append(object)  #или то или другое надо
                                #open('newyeartree.txt', 'a').write(str(obj2dict(object)) + "\n")
                                #self._get_events_sender().send_game_events([{"itemId":"CAKE_PACK_FREE1","action":"remoteNewYear","type":"item","objId":object.id}])
                                countnyt += 1
                                #pass
                                #else: print "NO"
                        # Добавляем в список объекты для копания клада
                        if tf == 0:  # and alldigged == 0:
                            #favdecors = ['D_SAKURASMALL','D_REDTREE','D_CONIFER','D_GATE','D_STATUETTE']#мыло
                            #favdecors = ['D_EIFFEL','B_JAPAN','B_JAPAN_LAKE']#металолом
                            #favdecors = ['D_SHIP','D_POOL2','B_POOL','B_WHITEHOUSE','B_BUSINESS','D_IDOL2','D_FLOWER4_WHITE','D_FLOWER4_YELLOW','DS_SYMBOL_U_NESKL','DS_SYMBOL_I_BEL','D_OLYMPIAD_STATUE','B_FLAG_OLIMPIADA','B_VAN_ICE_CREAM','B_CUPOLA','B_CUPOLA_CASH','B_ZAPOROZHETS','B_ZAPOROZHETS_OLD','B_HUT_CH_LEGS','B_RUSALKA','B_YACHT']#брендовая колл.(флюгер)
                            #if any("abc" in s for s in favdecors):
                            fn = 0
                            for fdec in favdecors:
                                if object.item[0 - len(fdec):] == fdec:
                                    print u'####### Найдены декорации ########',
                                    print fdec
                                    objssvl.insert(0, object)
                                    break
                                elif object.type in objtypes:
                                    objssvl2.append(object)
                                    break
                        ft = 0
                        if object.type == "fruitTree": ft = 1
                        if tf == 0 and object.type == 'monsterPit' and object.state == 'DIGGING':  #это должен быть статус зарытости.
                            object.users = len(object.users)
                            #if not myid in object.users:#если не ошибаюсь так. 
                            #open('monster.txt', 'a').write(str(obj2dict(object))+"\n")
                            self.cprint(u'53Закапываем чудика#### ^7Закопало уже^6_%d^1человек' % (
                            object.users))  #там какоето количество юзеров
                            ###################self._get_events_sender().send_game_events([{"itemId":"MONSTER_PIT_1_INSTRUMENT_PACK_DEFAULT","action":"remoteMonsterPit","type":"item","objId":object.id}])
                            events.append(
                                {"itemId": "MONSTER_PIT_1_INSTRUMENT_PACK_DEFAULT", "action": "remoteMonsterPit",
                                 "type": "item", "objId": object.id})
                            mt = 3
                            #else: self.cprint(u'4Уже здесь был!!')
                        elif tf == 0 and object.type == 'monsterPit':
                            self.cprint(u'4МИШУТА ЗАРЫТ ПО САМОЕ НЕ БАЛУЙ)))')  #это будет красным))
                        if tf == 0:
                            pass  #open('objects.txt', 'a').write(str(obj2dict(object))+"\n")
                            #if hasattr(self._get_game_state(),'playersInfo'):
                            #open('objects.txt', 'a').write('-------------------------------------' + "\n")
                            #open('objects.txt', 'a').write(str(obj2dict(self._get_game_state().playersInfo))+"\n")

            try:
                mt
            except NameError:
                mt = 0
            #if mt == 3:open('monster.txt', 'a').write(str(friends[self._get_game_state().countfnyt-1])+"\n"+'-------------------------------------' + "\n")
            countlop = 0
            if len(objssvl) > 0 or len(objssvl2) > 0:
                if objssvl == []: objssvl = objssvl2
                for i in range(shovels):
                    objdig = random_number.choice(objssvl)
                    #######################self._get_events_sender().send_game_events([{"objId":objdig.id,"x":objdig.x,"action":"remoteDig","y":objdig.y,"type":"item"}])
                    events.append(
                        {"objId": objdig.id, "x": objdig.x, "action": "remoteDig", "y": objdig.y, "type": "item"})
                    countlop += 1
                SetConsoleTextAttribute(stdout_handle, 0x0004 | 0x0008)
                print u"Использовал: ",
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0007 | 0x0008)
                print str(countlop),
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0006 | 0x0008)
                print u" лопат"
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0002 | 0x0008)
            elif alldigged == 1:
                SetConsoleTextAttribute(stdout_handle, 0x0004 | 0x0008)
                print (u'Всё уже выкопано до меня!').encode('cp866')
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0002 | 0x0008)
            else:
                SetConsoleTextAttribute(stdout_handle, 0x0004 | 0x0008)
                print (u'Нечего копать!').encode('cp866')
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0002 | 0x0008)
            if countpickup > 0:
                SetConsoleTextAttribute(stdout_handle, 0x0005 | 0x0008)
                print u"Вскрыли сундуков: ",
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0006 | 0x0008)
                print str(countpickup)
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0002 | 0x0008)
            if hasattr(self._get_game_state(), 'alldigged'): del self._get_game_state().alldigged
            del self._get_game_state().gameObjects
            self._get_game_state().countnyt += countnyt
            if not hasattr(self._get_game_state(), 'nytend') and countnyt > 0:
                for nyt in newyeartree:  # хотя тут и не работает. попробуем без ёлок.
                    #pass##############self._get_events_sender().send_game_events([{"itemId":"CAKE_PACK_FREE1","action":"remoteNewYear","type":"item","objId":nyt.id}])
                    events.append(
                        {"itemId": "CAKE_PACK_FREE1", "action": "remoteNewYear", "type": "item", "objId": nyt.id})
                try:
                    coucake = int(open('.\counts\\' + curuser + '\couCAKE.txt').read()) + len(newyeartree)
                except:
                    coucake = 1
                open('.\counts\\' + curuser + '\couCAKE.txt', 'w').write(str(coucake))
                SetConsoleTextAttribute(stdout_handle, 0x0006 | 0x0008)
                print u"поЛожил пряник(ов): ",
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0007 | 0x0008)
                print str(self._get_game_state().sendNewYearGift) + ":" + str(
                    self._get_game_state().countnyt) + "/" + str(countnyt) + " dub: " + str(countmyg)
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0002 | 0x0008)
            ################self._get_events_sender().send_game_events([{"action":"remoteFertilizeFruitTree","type":"item"}])
            try:
                couFruitTree = int(open('.\counts\\' + curuser + '\couFruitTree.txt').read()) + 1
            except:
                couFruitTree = 1
            if ft == 1 and couFruitTree < 20:
                print u'Удобряем деревья!'
                events.append({"action": "remoteFertilizeFruitTree", "type": "item"})
                open('.\counts\\' + curuser + '\couFruitTree.txt', 'w').write(str(couFruitTree))
            if events != []:
                self._get_events_sender().send_game_events(events)
            else:
                self.cprint('4EventsBlank')
            self._get_game_state().shovel = 0
            #if self._get_game_state().countfnyt + 1 > len(friends):sys.exit(0)
            if self._get_game_state().countfnyt < len(friends):
                user = friends[self._get_game_state().countfnyt]
                try:
                    isles = (open('.\subloc\\' + str(user) + '.txt').read()).split("\n")
                except:
                    isles = []
                #------------------------------------------------
                try:
                    locations = self._get_game_state().locationInfos
                except:
                    locations = None
                #open('locationInfos.txt', 'a').write(str(user)+"\n")
                #open('locationInfos.txt', 'a').write(str(locations)+"\n")
                if 0:  #hasattr(self._get_game_state(), 'playersInfo'):
                    playersInfo = self._get_game_state().playersInfo
                    #for playerInfo in playersInfo:
                    #    if hasattr(playerInfo, 'playerStatus'):
                    #wishlist = []
                    wishlist = "[ "
                    wishlist2 = "[ "
                    for wish in playersInfo[0].liteGameState.wishlist:
                        if wish != None:
                            if not wish in ['@CR_70', '@CR_44', '@CR_25', '@CR_16', '@CR_06']:
                                wishlist += self._get_item_reader().get(wish).name + ", "
                            else:
                                wishlist2 += self._get_item_reader().get(wish).name + ", "
                            #wishlist.append((self._get_item_reader().get(wish).name).encode('utf-8', 'ignore'))
                            wishlist += self._get_item_reader().get(wish).name + ", "
                            #except:wishlist.append("N/A")
                    wishlist += " ]"
                    wishlist2 += " ]"
                    wishlist = wishlist.encode('cp1251', 'ignore')  #wishlist.encode('UTF-8', 'ignore')
                    wishlist2 = wishlist2.encode('cp1251', 'ignore')
                    open('playersInfo.txt', 'a').write(
                        str(playersInfo[0].id) + wishlist + "   " + wishlist2 + "\n" + "\n")

                #open('.\usersState\\'+str(user)+'.txt', 'w').write(str(obj2dict(self._get_game_state()))+"\n")
                try:
                    isle = isles[self._get_game_state().couisle]
                except:
                    isle = 'main'
                if isle == '':
                    self._get_game_state().countfnyt += 1
                    open('.\counts\\' + curuser + '\countfnyt.txt', 'w').write(str(self._get_game_state().countfnyt))
                    self._get_game_state().couisle = 0
                    isle = 'main'
                    user = friends[self._get_game_state().countfnyt]
                self.cprint(u'1######### Идем к другу^6_%s^7_%d/%d^6на^3%s^1#########' % (
                user, self._get_game_state().countfnyt + 1, len(friends), isle))
                if self._get_game_state().couisle >= len(isles):
                    self._get_game_state().countfnyt += 1
                    open('.\counts\\' + curuser + '\countfnyt.txt', 'w').write(str(self._get_game_state().countfnyt))
                    self._get_game_state().couisle = 0
                else:
                    self._get_game_state().couisle += 1
                open('.\counts\\' + curuser + '\couisle.txt', 'w').write(str(self._get_game_state().couisle))
                print str(self._get_game_state().couisle) + "/" + str(len(isles))
                #print self._get_game_state().mylocids
                #open('locids.txt', 'w').write(str(self._get_game_state().get_state().locationInfos))
                self._get_events_sender().send_game_events([
                    {"action": "gameState", "locationId": isle, "user": str(user), "objId": None, "type": "gameState"}])
                #self._get_events_sender().send_game_events([{"action":"gameState","locationId":"main","user":str(user),"objId":None,"type":"gameState"}])#,{"type":"players","action":"getInfo","players":[str(user)]}])
                #self._get_game_state().countfnyt += 1
                #open('countfnyt.txt', 'w').write(str(self._get_game_state().countfnyt))
        if self._get_game_state().countfnyt >= len(friends):
            #print '######## END #########'
            if not hasattr(self._get_game_state(), 'fdend'):
                self._get_game_state().fdend = 1
                SetConsoleTextAttribute(stdout_handle, 0x0004 | 0x0008)
                print u'                               Возвращаемся домой                              '
                sys.stdout.flush()
                SetConsoleTextAttribute(stdout_handle, 0x0002 | 0x0008)
                self._get_events_sender().send_game_events(
                    [{"action": "gameState", "locationId": "main", "type": "gameState"}])  
          